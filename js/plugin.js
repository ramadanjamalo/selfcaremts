$(document).ready(function() {
  $('.toggle-class').click(function() {

    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar-open').css('display', 'none');
      $('.sidebar-closed').css('display', 'block');

    } else {
      $('.sidebar-open').css('display', 'block');
      $('.sidebar-closed').css('display', 'none');
    }

  });

  $('.variable-width').slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    fade: true,
    cssEase: 'linear',
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
});
